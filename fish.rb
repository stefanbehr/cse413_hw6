# Stefan Behr
# CSE 413
# Homework 6
# 11/20/2012

class Card
    attr_accessor :rank, :suit

    def initialize(rank, suit)
        @rank = rank
        @suit = suit
    end

    def to_s
        "[#{@rank} of #{@suit}]"
    end
end

class Deck
    attr_accessor :cards

    def initialize(ranks, suits)
        @cards = []
        ranks.each{|rank|
            suits.each{|suit|
                card = Card.new(rank, suit)
                # automatic shuffling
                slot = rand(@cards.length).floor # random int in [0, @cards.length)
                @cards.insert(slot, card)
            }
        }
    end

    def draw
        # draw one card
        @cards.pop
    end
    
    def deal(n)
        # deal n cards (usu. a hand)
        hand = []
        cand = -1 # index of first non-duplicate candidate
        while hand.length < n
            if not hand.map {|card| card.rank}.include?(@cards[cand].rank)
                hand.push(@cards.delete_at(cand))
                cand = -1
            else
                cand -= 1 # try next possible candidate card
            end
        end
        hand
    end

    def to_s
        @cards.join("\n")
    end
    
    def size
        @cards.length
    end
end

class Hand
    attr_accessor :player, :cards

    def initialize(player, cards)
        @player = player
        @cards = cards
    end

    def addcard(card)
        @cards.push(card)
    end

    def rankcount(rank)
        @cards.count {|card| card.rank == rank}
    end

    def hasrank?(rank)
        self.rankcount(rank) > 0
    end
    
    def haspair?(rank)
        self.rankcount(rank) > 1
    end

    def removeoneofrank(rank)
        match = @cards.select {|card| card.rank == rank}.first
        @cards.delete(match)
    end

    def removepair(rank)
        2.times {self.removeoneofrank(rank)}
    end

    def randomcard
        i = rand(@cards.length)
        @cards[i]
    end

    def empty?
        @cards.length == 0
    end

    def size
        @cards.length
    end

    def to_s
        @cards.join(" ")
    end
end

class Game
    attr_accessor :deck, :human, :computer, :winner

    @@facecards = Hash.new {|hash, card| hash[card.slice(0, 1)] = card}
    ['jack', 'queen', 'king', 'ace'].each {|x| @@facecards[x]}
    @@numcards = 2.upto(10).to_a.map {|n| n.to_s}
    @@ranks = @@numcards + @@facecards.values
    @@suits = ['hearts', 'spades', 'diamonds', 'clubs']
    @@moves = @@numcards + @@facecards.keys
    
    @@commands = ['n', 'x', 'h']
    @@help = [  "All moves and commands are case-insensitive!",
                "Valid card requests (bracketed letters are moves):",
                "#{(@@numcards +
                    @@facecards.values.sort.map {|s|
                                                "[#{s.slice(0, 1)}]" +
                                                s.slice(1, s.length)}
                    ).map {|s| "  "+s}.join("\n")}",
                "'h' command: prints this help message",
                "'n' command: starts new game",
                "'x' command: exits game"].join("\n")

    def self.ranks
        @@ranks
    end
    
    def self.suits
        @@suits
    end
    
    def self.commands
        @@commands
    end

    def self.moves
        @@moves
    end

    def self.facecards
        @@facecards
    end

    def self.help
        @@help
    end
    
    def initialize
        @deck = Deck.new(self.class.ranks, self.class.suits)
        @winner = nil

        print "Human, please enter your name: "
        pname = gets().strip.downcase

        @human = Hand.new(pname, @deck.deal(7))
        @computer = Hand.new("computer", @deck.deal(7))

        puts "Thank you, #{@human.player}, we can begin now."
    end
    
    def showhelp
        puts self.class.help
    end
end

def play
    # set up game, ready to go
    game = Game.new

    continue = true
    while continue # session loop (multiple games)
        while true # game loop (single game)
            puts "\nYour hand, #{game.human.player}:\n#{game.human}"
            puts "The computer has #{game.computer.size} cards"
            print "Your move ('h' for help): "

            move = gets().strip.downcase
            puts

            if move == 'x'
                exit
            elsif move == 'n'
                game = Game.new
                next
            elsif move == 'h'
                game.showhelp
                next
            elsif game.class.moves.include?(move) # valid move
                # expand move to name of face card if needed
                if game.class.facecards.include?(move)
                    move = game.class.facecards[move]
                end

                # ensure that player asks only for rank they have
                if not game.human.hasrank?(move)
                    puts "You must ask for a rank you already have! Try again."
                    next
                end

                # check opponent's hand for requested rank, draw or transfer
                if game.computer.hasrank?(move)
                    card = game.computer.removeoneofrank(move)
                    puts "Ooh, you got #{game.computer.player}'s card!"
                    if game.computer.empty?
                        game.winner = game.computer.player
                        break
                    end
                else
                    puts "Go fish, #{game.human.player}!"
                    card = game.deck.draw
                end

                puts "#{game.human.player} is getting #{card}..."

                # add drawn/transferred card to hand, discard if pairs
                if game.human.hasrank?(card.rank)
                    puts "You have a pair of #{card.rank}s! Discarding..."
                    game.human.removeoneofrank(card.rank)
                    if game.human.empty?
                        game.winner = game.human.player
                        break
                    end
                else
                    # not a duplicate, add to hand
                    game.human.addcard(card)
                end
            else
                puts "Invalid command, try again."
                next
            end

            # computer's turn (i would abstract most of both player's turns into a
            # single function, but i would have abandon my dependence on break, and
            # i'd prefer to keep that

            puts "\n#{game.computer.player}'s turn!\n"

            # choose random card from computer's hand (and get rank)
            c_rank = game.computer.randomcard.rank
            puts "#{game.computer.player} is asking for a #{c_rank}..."

            # get card from human if present, otherwise draw
            if game.human.hasrank?(c_rank)
               card = game.human.removeoneofrank(c_rank)
               puts "Ooh, #{game.computer.player} got your card!"
               if game.human.empty?
                  game.winner = game.human.player
                  break
               end
            else
               puts "Go fish, #{game.computer.player}!"
               card = game.deck.draw
            end

            # check if received card will form pair, discard if so, otherwise keep
            if game.computer.hasrank?(card.rank)
               puts "#{game.computer.player} has a pair of #{card.rank}s! Discarding..."
               game.computer.removeoneofrank(card.rank)
               if game.computer.empty?
                  game.winner = game.computer.player
                  break
               end
            else
               game.computer.addcard(card)
            end
        end
    
        # this means someone has won and broken out
        puts "*** #{game.winner.upcase} WON! ***"
        print "\n'n' for new game, 'x' to exit: "
        command = gets().strip.downcase

        # guard against bad input
        while command != "n" and command != "x"
              print "Invalid input, use 'x' to exit or 'n' for new game: "
              command = gets().strip.downcase
        end

        case command
        when "x"
             continue = false
        when "n"
             game = Game.new
        end
    end
end

play