Stefan Behr
CSE 413
Homework 6
11/21/2012

Howto
-----
Launch the game using 'ruby fish.rb' at the command-line. Once in the game, you will be prompter for a name. Enter it and hit the return key, and play will commence. I implemented the game so that it will respond to all required commands/moves without case sensitivity. 'x' will exit, 'n' will restart the game, etc.

Additions: The only additional command that I implemented is 'h' (or 'H'), which prints out a short help menu.

Extensions
----------
I only added a simple extension to my game. I restricted the human player to requesting only cards whose rank already appears in the player's hand, per the suggestion of the assignment.
